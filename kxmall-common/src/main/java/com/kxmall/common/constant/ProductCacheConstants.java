package com.kxmall.common.constant;

/**
 * 商品相关缓存key
 *
 * @author 郅兴开源团队-小黑
 */
public interface ProductCacheConstants {

   /**
    * 广告
    */
   String ADVERTISEMENT_NAME = "ADVERTISEMENT_TYPE_";


   /**
    * 推荐
    */
   String RECOMMEND_NAME = "RECOMMEND_TYPE_";


   /**
    * 仓库缓存key
    */
   String STORAGE_INFO_PREFIX = "STORAGE_INFO_";


   /**
    * 评价key
    */
   String CA_APPRAISE_KEY = "CA_APPRAISE_";
}
