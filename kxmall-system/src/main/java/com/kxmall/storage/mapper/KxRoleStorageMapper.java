package com.kxmall.storage.mapper;

import com.kxmall.common.core.mapper.BaseMapperPlus;
import com.kxmall.storage.domain.KxRoleStorage;
import com.kxmall.storage.domain.vo.RoleStorageVo;

/**
 * 仓库权限管理Mapper接口
 *
 * @author kxmall
 * @date 2023-08-27
 */
public interface KxRoleStorageMapper extends BaseMapperPlus<KxRoleStorageMapper, KxRoleStorage, RoleStorageVo> {



}
