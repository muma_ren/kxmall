package com.kxmall.storage.mapper;

import com.kxmall.common.core.mapper.BaseMapperPlus;
import com.kxmall.storage.domain.KxOutStockProduct;
import com.kxmall.storage.domain.vo.KxOutStockProductVo;

/**
 * 出库商品Mapper接口
 *
 * @author 郅兴开源团队-小黑
 * @date 2023-08-29
 */
public interface KxOutStockProductMapper extends BaseMapperPlus<KxOutStockProductMapper, KxOutStockProduct, KxOutStockProductVo> {

}
