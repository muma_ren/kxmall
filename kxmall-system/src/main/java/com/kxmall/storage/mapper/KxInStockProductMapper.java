package com.kxmall.storage.mapper;

import com.kxmall.common.core.mapper.BaseMapperPlus;
import com.kxmall.storage.domain.KxInStockProduct;
import com.kxmall.storage.domain.vo.KxInStockProductVo;

/**
 * 入库商品Mapper接口
 *
 * @author 郅兴开源团队-小黑
 * @date 2023-08-29
 */
public interface KxInStockProductMapper extends BaseMapperPlus<KxInStockProductMapper, KxInStockProduct, KxInStockProductVo> {

}
