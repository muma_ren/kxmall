package com.kxmall.activity.mapper;


import com.kxmall.activity.domain.KxStoreActivity;
import com.kxmall.activity.domain.vo.KxStoreActivityVo;
import com.kxmall.common.core.mapper.BaseMapperPlus;

/**
 * 活动商品Mapper接口
 *
 * @author kxmall
 * @date 2024-08-07
 */
public interface KxStoreActivityMapper extends BaseMapperPlus<KxStoreActivityMapper, KxStoreActivity, KxStoreActivityVo> {

}
