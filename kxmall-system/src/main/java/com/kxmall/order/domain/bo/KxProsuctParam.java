package com.kxmall.order.domain.bo;

import lombok.Data;

/**
 * @author :
 */
@Data
public class KxProsuctParam {

    /**
     * 商品id
     */
    private Long productId;

}
