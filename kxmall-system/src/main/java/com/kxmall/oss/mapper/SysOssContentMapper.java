package com.kxmall.oss.mapper;

import com.kxmall.common.core.mapper.BaseMapperPlus;
import com.kxmall.oss.domain.SysOssContent;

/**
 * 文件Mapper接口
 *
 * @author kxmall
 * @date 2024-12-07
 */
public interface SysOssContentMapper extends BaseMapperPlus<SysOssContentMapper, SysOssContent, SysOssContent> {

}
